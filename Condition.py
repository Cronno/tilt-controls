class Condition:

    def __init__(self, maxMatches, maxLosses, maxConLosses):
        self.maxMatches = maxMatches
        self.maxLosses = maxLosses
        self.maxConLosses = maxConLosses

    def check(self, counter):
        if counter.getLosses() >= self.maxLosses:
            return False
        if counter.getWins() + counter.getLosses() >= self.maxMatches:
            return False
        if counter.getConLosses() >= self.maxConLosses:
            return False
        return True

    def nextLoss(self, counter):
        self.maxLosses = counter.getLosses() + 1

    def last(self, counter):
        self.maxMatches = counter.getWins() + counter.getLosses() + 1
