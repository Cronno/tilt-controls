from tkinter import *
from tkinter import ttk
from PIL import ImageTk
import Counter
import Condition
import History

class GUI:

    def __init__(self, root):
        self.root = root
        self.currentWindow = None
        self.images = []

    def startUI(self):
        if self.currentWindow != None:
            self.currentWindow.destroy()
        self.currentWindow = ttk.Frame(self.root)
        self.currentWindow.grid(column=0, row=0, sticky=(N, W, E, S))
        self.currentWindow.rowconfigure([0,1,2], weight=1, minsize=50)
        self.currentWindow.columnconfigure([0], weight=1, minsize=50)

        title = StringVar(value="untitled")
        titleEntry = ttk.Entry(self.currentWindow, textvariable=title, width=10)
        titleEntry.grid(row=0, column=0)

        optionFrame = ttk.Frame(self.currentWindow)
        optionFrame.grid(row=1, column=0)
        optionFrame.rowconfigure([0,1,2], weight=1, minsize=50)
        optionFrame.columnconfigure([0,1,2], weight=1, minsize=50)
        # OptionVals [[Boolean for checkbox, Value in entry], ...]
        optionVals = []
        optionVals.append(self.createOption(optionFrame, "Stop after", "matches", 0))
        optionVals.append(self.createOption(optionFrame, "Stop after", "losses", 1))
        optionVals.append(self.createOption(optionFrame, "Stop after", "consecutive losses", 2))
        btn_start = ttk.Button(self.currentWindow, text="Start", command=lambda: self.startButton(title, optionVals))
        btn_start.grid(row=2, column=0)

    def startButton(self, title, optionVals):
        # if the box was not checked or the number is 0, make the value infinity
        for option in optionVals:
            if not option[0].get() or option[1].get() == 0:
                option[1] = DoubleVar(value=float('inf'))
        self.cond = Condition.Condition(optionVals[0][1].get(), optionVals[1][1].get(), optionVals[2][1].get())
        self.title = title.get()
        self.mainUI()

    def mainUI(self):
        self.counter = Counter.Counter()
        self.currentWindow.destroy()
        self.currentWindow = ttk.Notebook(self.root)
        self.currentWindow.grid(column=0, row=0, sticky=(N, W, E, S))
        self.counterFrame = self.counterTab(self.currentWindow)
        historyFrame = self.historyTab(self.currentWindow)
        self.currentWindow.add(self.counterFrame, text="Counter")
        self.currentWindow.add(historyFrame, text="History")

        # Key bindings
        self.root.bind("<KeyPress-w>", self.winBtn)
        self.root.bind("<KeyPress-l>", self.lossBtn)

    def counterTab(self, parent):
        counterFrame = ttk.Frame(self.currentWindow)
        counterFrame.rowconfigure([0,1,2,3], weight=1, minsize=50)
        counterFrame.columnconfigure([0,1,2], weight=1, minsize=50)

        # Labels on counterUI
        self.lbl_title = ttk.Label(counterFrame, text=self.title)
        self.lbl_title.grid(row=0, column=1)

        s = ttk.Style()
        s.configure('record.TLabel', font='TkDefaultFont 50')
        self.record = StringVar(value="W0-L0")
        lbl_record = ttk.Label(counterFrame, textvariable=self.record, style='record.TLabel')
        lbl_record.grid(row=1, column=0, columnspan=3)

        self.rate = StringVar(value="--%")
        self.lbl_rate = ttk.Label(counterFrame, textvariable=self.rate)
        self.lbl_rate.grid(row=2, column=2)

        # Buttons on CounterUI
        btn_win = ttk.Button(counterFrame, text="Win", command=self.winBtn)
        btn_win.grid(row=3, column=0, sticky=(N, W, E, S))

        btn_loss = ttk.Button(counterFrame, text="Loss", command=self.lossBtn)
        btn_loss.grid(row=3, column=2, sticky=(N, W, E, S))

        frame_midBtns = ttk.Frame(counterFrame)
        frame_midBtns.grid(row=3, column=1, sticky=(N, W, E, S))
        frame_midBtns.rowconfigure([0,1,2], weight=1, minsize=0)
        frame_midBtns.columnconfigure(0, weight=1, minsize=0)

        btn_nextL = ttk.Button(frame_midBtns, command=lambda: self.cond.nextLoss(self.counter))
        btn_nextL["text"] = "End on Next Loss"
        btn_nextL.grid(row=0, column=0, sticky=(N, W, E, S))

        btn_last = ttk.Button(frame_midBtns, text="Last Match", command=lambda: self.cond.last(self.counter))
        btn_last.grid(row=1, column=0, sticky=(N, W, E, S))

        btn_end = ttk.Button(frame_midBtns, text="End", command=self.end)
        btn_end.grid(row=2, column=0, sticky=(N, W, E, S))
        return counterFrame

    def historyTab(self, parent):
        historyFrame = ttk.Frame(parent)
        historyFrame.grid(sticky=(N,S,E,W))
        historyFrame.rowconfigure([0], weight=0, minsize=25)
        historyFrame.rowconfigure([1], weight=1)
        historyFrame.columnconfigure([0], weight=1)

        titleLabel = ttk.Label(historyFrame, text=self.title)
        titleLabel.grid(row=0, column=0)

        # Canvas for scrolling
        canvas = Canvas(historyFrame)
        canvas.grid(row=1, column=0, sticky=(N,S,E,W))
        canvas.rowconfigure([0], weight=1)
        canvas.columnconfigure([0], weight=1)
        s = ttk.Scrollbar(historyFrame, orient=VERTICAL, command=canvas.yview)
        s.grid(row=1, column=1, sticky=(N,S))
        canvas.configure(yscrollcommand=s.set)

        # Container Frame inside canvas holds all histories and charts
        histContainer = ttk.Frame(canvas)
        canvas.create_window((0,0), window=histContainer, anchor="nw")
        histContainer.bind("<Configure>", lambda event: canvas.configure(scrollregion=canvas.bbox("all")))
        canvas.bind_all("<MouseWheel>", lambda event: canvas.yview_scroll(-1*(event.delta//40), "units"))
        canvas['width'] = histContainer['width']

        self.history = History.History(self.title)
        self.images = [0]
        totalWL = ttk.Label(histContainer, text=self.history.WLstr())
        totalWL.grid(row=0, column=0)
        self.currentHist = self.createHistFrame(histContainer, self.counter, 1)
        row = 2
        for counter in self.history.counterList:
            self.createHistFrame(histContainer, counter, row)
            row += 1

        return historyFrame

    def createHistFrame(self, parent, counter, row):
        frame = ttk.Labelframe(parent, text=counter.date.strftime("%x"))
        frame.grid(row=row, column=0, sticky=(N,S,E,W), pady=10, padx=30)
        frame.rowconfigure([0,1], weight=1)
        frame.columnconfigure([0,1], weight=1)

        recordLabel = ttk.Label(frame, text=counter.WLstr())
        recordLabel.grid(row=0, column=0)
        rateLabel = ttk.Label(frame, text=str(counter.calcRate()) + "%")
        rateLabel.grid(row=0, column=1)

        chartLabel = ttk.Label(frame)
        chartLabel.grid(row=1,column=0, columnspan=2)
        if row != 1:
            self.images.append(ImageTk.PhotoImage(counter.createChart()))
            chartLabel['image'] = self.images[-1]
        else:
            # The image at the top of the list is current and should be replaced instead of appended
            self.images[0] = ImageTk.PhotoImage(counter.createChart())
            chartLabel['image'] = self.images[0]
        return frame

    def refresh(self):
        # update W-L and win rate displays
        self.record.set(self.counter.WLstr())
        self.rate.set(str(self.counter.calcRate()) + '%')
        # update the chart for the current history
        parent = self.currentHist.master
        self.currentHist.destroy()
        self.currentHist = self.createHistFrame(parent, self.counter, 1)
        if not self.cond.check(self.counter):
            self.end()

    def end(self):
        self.history.outputHistory(self.counter)
        self.root.unbind("<KeyPress-w>")
        self.root.unbind("<KeyPress-l>")
        for widget in self.counterFrame.grid_slaves(row=3):
            widget.destroy()
        resetBtn = ttk.Button(self.counterFrame, text="Reset", command=self.startUI)
        resetBtn.grid(row=3, column=0, columnspan=3, sticky=(N, W, E, S))

    def winBtn(self, *args):
        self.counter.addWin()
        self.refresh()

    def lossBtn(self, *args):
        self.counter.addLoss()
        self.refresh()

    def toggleState(self, bool, widget):
        if bool:
            widget.state(['!disabled'])
        else:
            widget.state(['disabled'])

    def createOption(self, parent, option, end, rowNum):
        checkboxBool = BooleanVar()
        checkBox = ttk.Checkbutton(parent, variable=checkboxBool, onvalue=True, offvalue=False)
        checkBox.grid(row=rowNum, column=0, sticky=(W))
        checkBox["text"] = option
        checkBox["command"] = lambda: self.toggleState(checkboxBool.get(), entry)

        entryVar = IntVar()
        entry = ttk.Entry(parent, textvariable=entryVar, state="disabled", width=5)
        entry.grid(row=rowNum, column=1)

        endLabel = ttk.Label(parent, text=end)
        endLabel.grid(row=rowNum, column=2, sticky=(W))
        return [checkboxBool, entryVar]
