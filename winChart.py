from PIL import Image, ImageDraw
import math

# Define colors
WIN = (49,224,96)
LOSS = (229,70,61)
GRID = (66,70,81)
BORDER = (0,0,0)
BORDER_T = BORDER+(96,)

def create(history, width=10):
    total = sum(history)
    height = math.ceil(total/width)
    if height == 0:
        height = 1
    chart = createChart(width, height, history)
        
    # Scale chart (each square ends up scale-1 x scale-1)
    scale = 21
    chart = chart.resize((chart.width*scale+1, chart.height*scale+1), Image.NEAREST)

    # Add grid
    grid = createGrid(width, height, scale)
    return Image.alpha_composite(chart, grid)

def createChart(width, height, history):
    chart = Image.new("RGBA", (width,height), (128,128,128))
    x = 0
    y = 0
    win=True
    draw = ImageDraw.Draw(chart)
    for count in history:
        if win:
            color = WIN
        else:
            color = LOSS
        remaining = count
        while remaining > 0:
            if x + remaining < width:
                # The remaining wins/losses fit in the current row
                draw.line([(x,y),(x+remaining-1,y)],color)
                x = x + remaining
                remaining = 0
            else:
                # Current streak does not fit in the row, finish row and move down
                draw.line([(x,y),(width,y)],color)
                remaining -= width-x
                x = 0
                y += 1
        win = not win
    return chart

def createGrid(width, height, scale, majorX=5, majorY=5):
    # Create grid image
    scaledW = width*scale+1
    scaledH = height*scale+1
    grid = Image.new("RGBA", (width*scale+1,height*scale+1), (0,0,0,0))
    grid2 = Image.new("RGBA", (width*scale+1,height*scale+1), (0,0,0,0))
    draw = ImageDraw.Draw(grid)
    draw2 = ImageDraw.Draw(grid2)
    for i in range(height+1):
        position = i*scale
        draw.line([(0,position),(scaledW,position)], GRID)
    for i in range(width+1):
        position = i*scale
        draw.line([(position,0),(position,scaledH)], GRID)
    # Draw darker gridlines and borders
    for i in range(0, height+1, majorX):
        position = i*scale
        draw2.line([(0,position-1),(scaledW,position-1)], BORDER_T)
        draw.line([(0,position),(scaledW,position)], BORDER)
        draw2.line([(0,position+1),(scaledW,position+1)], BORDER_T)
    for i in range(0, width+1, majorY):
        position = i*scale
        draw2.line([(position-1,0),(position-1,scaledH)], BORDER_T)
        draw.line([(position,0),(position,scaledH)], BORDER)
        draw2.line([(position+1,0),(position+1,scaledH)], BORDER_T)
    draw2.line([(0,scaledH-2),(scaledW,scaledH-2)], BORDER_T)
    draw.line([(0,scaledH-1),(scaledW,scaledH-1)], BORDER)
    return Image.alpha_composite(grid,grid2)
