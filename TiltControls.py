from tkinter import *
import GUI

root = Tk()
root.title("TiltControls")
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
root.minsize(300,320)
root.iconbitmap('icon.ico')
gui = GUI.GUI(root)
gui.startUI()
root.mainloop()
