import json
import Counter
import datetime
from pathlib import Path
from file_read_backwards import FileReadBackwards

class History:

    def __init__(self, title):
        self.title = title
        self.counterList = []
        self.totalWins = 0
        self.totalLosses = 0
        self.loadHistory()
    
    def loadHistory(self):
        path = Path("logs/"+self.title+".log")
        if not path.exists():
            return
        with FileReadBackwards(path, "utf-8") as logFile:
            for line in logFile:
                hist = json.loads(line)
                date = datetime.datetime.fromtimestamp(hist[0])
                self.totalWins += hist[1]
                self.totalLosses += hist[2]
                self.counterList.append(Counter.Counter(date,hist[1],hist[2],hist[3]))
                
    def outputHistory(self, counter):
        if counter.wins == 0 and counter.losses == 0:
            return
        path = Path("logs")
        if not path.exists():
            path.mkdir(parents=True)
        with open(path/(self.title+".log"), 'a') as logFile:
            out = str([counter.date.timestamp(),counter.wins,counter.losses,counter.history])+'\n'
            logFile.write(out)

    def WLstr(self):
        return "W" + str(self.totalWins) + "-L" + str(self.totalLosses)