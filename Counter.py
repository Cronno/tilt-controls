import datetime
import WinChart

class Counter:

    def __init__(self, date=datetime.datetime.today(), wins=0, losses=0, history=None):
        self.date = date
        self.wins = wins
        self.losses = losses
        # history = [consecutive wins, consecutive losses, consecutive wins, ...]
        if history != None:
            self.history = history
        else:
            self.history = [0]
        self.chart = None

    def calcRate(self):
        if self.wins+self.losses > 0:
            return round(self.wins/(self.wins+self.losses) * 100, 2)
        else:
            return 100

    def addWin(self):
        self.wins += 1
        self.updateHistory("W")

    def addLoss(self):
        self.losses += 1
        self.updateHistory("L")

    def getWins(self):
        return self.wins

    def getLosses(self):
        return self.losses

    def updateHistory(self, result):
        # Result of the previous match can be found from the length of the history list
        if len(self.history) % 2 == 1:
            prev = 'W'
        else:
            prev = 'L'

        if prev == result:
            self.history[-1] += 1
        else:
            self.history.append(1)

    def getConLosses(self):
        if len(self.history) % 2 == 1:
            return 0
        else:
            return self.history[-1]

    def createChart(self):
        self.chart = WinChart.create(self.history)
        return self.chart

    def WLstr(self):
        return "W" + str(self.wins) + "-L" + str(self.losses)
